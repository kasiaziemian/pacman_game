import pygame

from player import *
from enemies import *
from settings import *
from animation import *
from menu import *

class Game(object):
    def __init__(self):
        self.font = pygame.font.Font(None, 40)
        self.about = False
        self.game_over = True
        self.score = 0
        self.menu = Menu(("start game", "about", "quit"), font_color = WHITE, font_size=60)
        self.player = Player(32, 128, "assets/player.png")
        self.horizontal_blocks = pygame.sprite.Group()
        self.vertical_blocks = pygame.sprite.Group()
        self.coins = pygame.sprite.Group()
        

        for i,row in enumerate(enviroment()):
            for j,item in enumerate(row):
                if item == 1:
                    self.horizontal_blocks.add(Block(j *32 + 8, i * 32 + 8, BLACK, 16, 16))
                elif item == 2:
                    self.vertical_blocks.add(Block(j * 32 + 8, i * 32 + 8, BLACK, 16, 16))

        self.enemies = pygame.sprite.Group()
        self.enemies.add(Ghost(288, 96, 0, 2))
        self.enemies.add(Ghost(288, 320, 0, -2))
        self.enemies.add(Ghost(544, 128, 0, 2))
        self.enemies.add(Ghost(32, 224, 0, 2))
        self.enemies.add(Ghost(160, 64, 2, 0))
        self.enemies.add(Ghost(448, 64, -2, 0))
        self.enemies.add(Ghost(640, 448, 2, 0))
        self.enemies.add(Ghost(448, 320, 2, 0))
    
        for i, row in enumerate(enviroment()):
            for j, item in enumerate(row):
                if item != 0:
                    self.coins.add(Ellipse(j * 32 + 12, i * 32 + 12, WHITE, 8, 8))


        self.pacman_sound = pygame.mixer.Sound("sound/pacman_sound.ogg")
        self.game_over_sound = pygame.mixer.Sound("sound/game_over_sound.ogg")


    def process_events(self):
        for event in pygame.event.get(): 
            if event.type == pygame.QUIT: 
                return True
            self.menu.event_handler(event)
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    if self.game_over and not self.about:
                        if self.menu.state == 0:
                            self.__init__()
                            self.game_over = False
                        elif self.menu.state == 1:
                            self.about = True
                        elif self.menu.state == 2:
                            return True

                elif event.key == pygame.K_RIGHT:
                    self.player.move_right()

                elif event.key == pygame.K_LEFT:
                    self.player.move_left()

                elif event.key == pygame.K_UP:
                    self.player.move_up()

                elif event.key == pygame.K_DOWN:
                    self.player.move_down()
                
                elif event.key == pygame.K_ESCAPE:
                    self.game_over = True
                    self.about = False

            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_RIGHT:
                    self.player.stop_move_right()
                elif event.key == pygame.K_LEFT:
                    self.player.stop_move_left()
                elif event.key == pygame.K_UP:
                    self.player.stop_move_up()
                elif event.key == pygame.K_DOWN:
                    self.player.stop_move_down()

            elif event.type == pygame.MOUSEBUTTONDOWN:
                self.player.explosion = True
                    
        return False

    def run_logic(self):
        if not self.game_over:
            self.player.update(self.horizontal_blocks, self.vertical_blocks)
            block_hit_list = pygame.sprite.spritecollide(self.player, self.coins, True)
            if len(block_hit_list) > 0:
                self.pacman_sound.play()
                self.score += 1
            block_hit_list = pygame.sprite.spritecollide(self.player, self.enemies, True)
            if len(block_hit_list) > 0:
                self.player.explosion = True
                self.game_over_sound.play()
            self.game_over = self.player.game_over
            self.enemies.update(self.horizontal_blocks, self.vertical_blocks)


    def display_frame(self,screen):
        screen.fill(BLACK)
        if self.game_over:
            if self.about:
                self.display_message(screen, "Pacman")
 
            else:
                self.menu.display_frame(screen)
        else:
            self.horizontal_blocks.draw(screen)
            self.vertical_blocks.draw(screen)
            draw_enviroment(screen)
            self.coins.draw(screen)
            self.enemies.draw(screen)

            screen.blit(self.player.image,self.player.rect)
            text = self.font.render("Score: " + str(self.score), True, GREEN)
            screen.blit(text, [120, 20])
            

        pygame.display.flip()

    def display_message(self, screen, message, color=RED):
        label = self.font.render(message, True, color=RED)
        width = label.get_width()
        height = label.get_height()
        dx = (SCREEN_WIDTH / 2) - (width / 2)
        dy = (SCREEN_HEIGHT / 2) - (height / 2)
        screen.blit(label,(dx, dy))



