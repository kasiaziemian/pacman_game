import pygame

from settings import *
from animation import *

class Player(pygame.sprite.Sprite):
    pos_x, pos_y = 0, 0
    explosion, game_over = False, False
    def __init__(self, x, y, fname):
        pygame.sprite.Sprite.__init__(self)

        self.image = pygame.image.load(fname).convert()
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.rect.topleft = (x, y)

        img = pygame.image.load("assets/walk.png").convert()

        self.move_right_animation = Animation(img, 32, 32)
        self.move_left_animation = Animation(pygame.transform.flip(img, True, False),32,32)
        self.move_up_animation = Animation(pygame.transform.rotate(img, 90), 32, 32)
        self.move_down_animation = Animation(pygame.transform.rotate(img, 270), 32, 32)
        
        img = pygame.image.load("assets/explosion.png").convert()
        
        self.explosion_animation = Animation(img, 30, 30)
        self.player_image = pygame.image.load(fname).convert()
        self.player_image.set_colorkey(BLACK)

    def update(self, horizontal_blocks, vertical_blocks):
        if not self.explosion:
            if self.rect.right < 0:
                self.rect.left = SCREEN_WIDTH
            elif self.rect.left > SCREEN_WIDTH:
                self.rect.right = 0
            if self.rect.bottom < 0:
                self.rect.top = SCREEN_HEIGHT
            elif self.rect.top > SCREEN_HEIGHT:
                self.rect.bottom = 0
    
            self.rect.x += self.pos_x
            self.rect.y += self.pos_y


            for block in pygame.sprite.spritecollide(self, horizontal_blocks, False):
                self.rect.centery = block.rect.centery
                self.change_y = 0
            for block in pygame.sprite.spritecollide(self, vertical_blocks, False):
                self.rect.centerx = block.rect.centerx
                self.pos_x = 0

            if self.pos_x > 0:
                self.move_right_animation.update(10)
                self.image = self.move_right_animation.get_current_image()
            elif self.pos_x < 0:
                self.move_left_animation.update(10)
                self.image = self.move_left_animation.get_current_image()

            if self.pos_y > 0:
                self.move_down_animation.update(10)
                self.image = self.move_down_animation.get_current_image()

            elif self.pos_y < 0:
                self.move_up_animation.update(10)
                self.image = self.move_up_animation.get_current_image()
        else:
            if self.explosion_animation.image_index == self.explosion_animation.get_length() -1:
                pygame.time.wait(500)
                self.game_over = True
                
            self.explosion_animation.update(12)
            self.image = self.explosion_animation.get_current_image()
            

    def move_right(self):
        self.pos_x = 3

    def move_left(self):
        self.pos_x = -3

    def move_up(self):
        self.pos_y = -3

    def move_down(self):
        self.pos_y = 3

    def stop_move_right(self):
        if self.pos_x != 0:
            self.image = self.player_image
        self.pos_x = 0

    def stop_move_left(self):
        if self.pos_x != 0:
            self.image = pygame.transform.flip(self.player_image, True, False)
        self.pos_x = 0

    def stop_move_up(self):
        if self.pos_y != 0:
            self.image = pygame.transform.rotate(self.player_image, 90)
        self.pos_y = 0

    def stop_move_down(self):
        if self.pos_y != 0:
            self.image = pygame.transform.rotate(self.player_image, 270)
        self.pos_y = 0





            
    
        
