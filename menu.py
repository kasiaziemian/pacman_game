import pygame

from game import *
from main import *
from player import *
from enemies import *
from animation import *
from settings import *

class Menu(object):
    def __init__(self, items, font_color=TRANSPARENT, select_color=RED, ttf_font=None, font_size=25, state=0):
        self.font_color = font_color
        self.select_color = select_color
        self.items = items
        self.font = pygame.font.Font(ttf_font, font_size)
        self.state = state

    def display_frame(self, screen):
        for index, item in enumerate(self.items):
            if self.state == index:
                label = self.font.render(item, True, self.select_color)
            else:
                label = self.font.render(item, True, self.font_color)
            
            width = label.get_width()
            height = label.get_height()
            
            total_height = len(self.items) * height
            dx = (SCREEN_WIDTH / 2) - (width / 2)
            dy = (SCREEN_HEIGHT / 2) - (total_height /2) + (index * height)
            
            screen.blit(label, (dx, dy))
        
    def event_handler(self,event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                if self.state > 0:
                    self.state -= 1
            elif event.key == pygame.K_DOWN:
                if self.state < len(self.items) -1:
                    self.state += 1