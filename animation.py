import pygame

from enemies import *
from settings import *
from game import *
from main import *
from player import *

class Animation(object):
    def __init__(self, img, width, height):
        self.sprite_sheet = img
        self.image_list = []
        self.load_images(width,height)
        self.image_index = 0
        self.clock = 1
        
    def load_images(self, width, height):
        for y in range(0, self.sprite_sheet.get_height(), height):
            for x in range(0, self.sprite_sheet.get_width(), width): 
                img = self.get_image(x, y, width, height)
                self.image_list.append(img)

    def get_image(self, x, y, width, height):
        image = pygame.Surface([width,height]).convert()
        image.blit(self.sprite_sheet,(0, 0),(x, y, width, height))
        image.set_colorkey(TRANSPARENT)
        return image

    def get_current_image(self):
        return self.image_list[self.image_index]

    def get_length(self):
        return len(self.image_list)

    def update(self, FPS):
        step = 30 // FPS
        l = range(1, 30,step)
        if self.clock == 30:
            self.clock = 1
        else:
            self.clock += 1

        if self.clock in l:
            self.image_index += 1
            if self.image_index == len(self.image_list):
                self.image_index = 0